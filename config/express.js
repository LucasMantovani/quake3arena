var expressValidator = require('express-validator');
var express = require('express')();
var consign = require('consign');

/**
* <summary>
* commonjs - modo padrão para carregamento de modulos 
* </summary>
*/
module.exports = function () {

    //Middleware
    express.use(expressValidator());


    //Consing ou poderia usar também o express-load
    //Ambos serve para que o express conheça todos os arquivos de uma determinada pasta
    consign()
        .include('controllers')
        .into(express);

    return express;
}
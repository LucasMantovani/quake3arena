var express = require('../config/express')();
var http = require('supertest')(express);

/**
 * <summary>
 * Teste unitário da nossa controller quakeLogParser 
 * </summary>
 */
describe('#quakeLogParserController', function () {

    //Tetes para o metodo que retorna todos os jogos

    it('#Listagem de todos os jogos, informando um arquivo existente.', function (done) {
        http.get('/quakelog')
            .set('filename', 'games.log')
            .expect(200, done);
    });

    it('#Listagem de todos os jogos, informando um arquivo não existente.', function (done) {
        http.get('/quakelog')
            .set('filename', 'gamess.log')
            .expect(404, done);
    });

    it('#Listagem de todos os jogos, informando um arquivo existente, porém vazio.', function (done) {
        http.get('/quakelog')
            .set('filename', 'fileEmpety.log')
            .expect(500, done);
    });

    it('#Listagem de todos os jogos, informando um arquivo existente, porém não é os logs do jogo Quake 3 Arena.', function (done) {
        http.get('/quakelog')
            .set('filename', 'fileNoQuake3Arena.log')
            .expect(500, done);
    });

    //Tetes para o método que retorna um jogo específico 

    it('#Listagem de um jogo específico, informando um arquivo existente, com um id válido.', function (done) {
        http.get('/quakelog/5')
            .set('filename', 'games.log')
            .expect(200, done);
    });

    it('#Listagem de um jogo específico, informando um arquivo não existente, com um id válido.', function (done) {
        http.get('/quakelog/5')
            .set('filename', 'gamess.log')
            .expect(404, done);
    });

    it('#Listagem de um jogo específico, informando um arquivo existente, porém vazio, com um id válido.', function (done) {
        http.get('/quakelog')
            .set('filename', 'fileEmpety.log')
            .expect(500, done);
    });

    it('#Listagem de um jogo específico, informando um arquivo existente, porém não é os logs do jogo Quake 3 Arena, com um id válido.', function (done) {
        http.get('/quakelog/5')
            .set('filename', 'fileNoQuake3Arena.log')
            .expect(500, done);
    });

    it('#Listagem de um jogo específico, informando um arquivo existente, com um id inválido.', function (done) {
        http.get('/quakelog/50')
            .set('filename', 'games.log')
            .expect(404, done);
    });

    it('#Listagem de um jogo específico, informando um arquivo existente, com um id com formato diferente de inteiro.', function (done) {
        http.get('/quakelog/id')
            .set('filename', 'games.log')
            .expect(400, done);
    });
})
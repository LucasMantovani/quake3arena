var readfile = require('../manager/quakeLogParserManager');
var fs = require('fs');

module.exports = function (app) {

    /**
    * <summary>
    * Método get 
    * É necessário que se passe no header o nome do arquivo Ex: filename/games.log
    * </summary>
    * <returns>retorna todos os jogos com informações agrupadas</returns>  
    */
    app.get('/quakelog', function (request, response) {

        //Verifica se o arquivo existe
        if (!fs.existsSync('./files/' + request.headers.filename)) {
            response.status(404).send('O arquivo informado no header não existe.');
        } else {

            new readfile(request.headers.filename, (rusult) => {

                //Verifica se o conteúdo veio vazio.
                // Possíveis erros: arquivo vazio ou não faz parte do game Quake 3 arena
                if (rusult.length == 0) {
                    response.status(500).send("Erro interno");
                }
                else {
                    response.status(200).json(rusult);
                }
            });
        }
    });

    /**
    * <summary>
    * Método get by id
    * É necessário que se passe no header o nome do arquivo Ex: filename/games.log
    * </summary>
    * <returns>retorna um jogo específico através do id</returns>  
    */
    app.get('/quakelog/:id', function (request, response) {

        //Validação o tipo do parâmetro que esta vindo
        request.assert('id', 'O parâmetro id deve ser um número inteiro').isInt();
        var error = request.validationErrors();

        if (error) {
            response.status(400).send(error);
        }
        else {
            //Verifica se o arquivo existe
            if (!fs.existsSync('./files/' + request.headers.filename)) {
                response.status(404).send('O arquivo informado no header não existe.');
            } else {

                new readfile(request.headers.filename, (rusult) => {


                    //Verifica se o conteúdo veio vazio.
                    // Possíveis erros: arquivo vazio ou não faz parte do game Quake 3 arena
                    if (rusult.length == 0) {
                        response.status(500).send("Erro interno");
                    }
                    else {
                        //Verifica se existe um jogo com o id que veio no parâmetro
                        if (rusult.length >= request.params.id) {
                            response.status(200).json(rusult[request.params.id - 1]);
                        } else {
                            response.status(404).send("Jogo não encontrado");
                        }
                    }
                });
            }
        }

    });
}
var app = require('./config/express')();
var cluster = require('cluster');
var os = require('os');


/**
 * <summary>
 * Escalabilidade.
 * Faz com que a API use todos os núcleos disponiveis do precessador, 
 * tornando a API mais escalavel.
 * A primeira coisa é verificar se é o master,  pois ele é o único que pode invocar o fork().
 * Fork -> Gera uma nova threads, filha da thread principal
 * </summary>
 */

if (cluster.isMaster) {
    os.cpus().forEach(function () {
        cluster.fork();
    });

    //Caso algum cluster desconectar 
    cluster.on('exit', worker => {
        cluster.fork();
    });

    // Se o cluster não for o master, ele deve somente executar o listen do express
} else {
    app.listen(5000, function () {
        console.log('Servidor rodando na porta 5000');
    });
}




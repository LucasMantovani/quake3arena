# Quake log parser

## Task 1 - Construa um parser para o arquivo de log games.log e exponha uma API de consulta.

O arquivo games.log é gerado pelo servidor de quake 3 arena. Ele registra todas as informações dos jogos, quando um jogo começa, quando termina, quem matou quem, quem morreu pq caiu no vazio, quem morreu machucado, entre outros.

O parser deve ser capaz de ler o arquivo, agrupar os dados de cada jogo, e em cada jogo deve coletar as informações de morte.

### Exemplo

      21:42 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT
  
  O player "Isgalamido" morreu pois estava ferido e caiu de uma altura que o matou.

      2:22 Kill: 3 2 10: Isgalamido killed Dono da Bola by MOD_RAILGUN
  
  O player "Isgalamido" matou o player Dono da Bola usando a arma Railgun.
  
Para cada jogo o parser deve gerar algo como:

    game_1: {
        total_kills: 45;
        players: ["Dono da bola", "Isgalamido", "Zeh"]
        kills: {
          "Dono da bola": 5,
          "Isgalamido": 18,
          "Zeh": 20
        }
      }



### Observações

1. Quando o `<world>` mata o player ele perde -1 kill.
2. `<world>` não é um player e não deve aparecer na lista de players e nem no dicionário de kills.
3. `total_kills` são os kills dos games, isso inclui mortes do `<world>`.

## Task 2 - Após construir o parser construa uma API que faça a exposição de um método de consulta que retorne um relatório de cada jogo.


# Requisitos

1. Use a linguagem que você tem mais habilidade (temos preferência por node.js, java, golang ou python, mas pode ser usado qualquer linguagem desde que explicado a preferência).
2. As APIs deverão seguir o modelo RESTFul com formato JSON  
3. Faça testes unitários, suite de testes bem organizados. (Dica. De uma atenção especial a esse item!)
4. Use git e tente fazer commits pequenos e bem descritos.
5. Faça pelo menos um README explicando como fazer o setup, uma explicação da solução proposta, o mínimo de documentação para outro desenvolvedor entender seu código
6. Siga o que considera boas práticas de programação, coisas que um bom desenvolvedor olhe no seu código e não ache "feio" ou "ruim".
7. Após concluir o teste suba em um repositório privado e nos mande o link

HAVE FUN :)

# Escolhas para desenvolver o desafio:
Para ser um diferencial eu escolhi uma linguagem que eu não dominava, que foi o Node.js, uma das preferências do desafio.
Hoje em dia a linguagem que eu tenho mais conhecimento é "Asp.Net Core", então resolvi encarar esses dois desafios, um seria
aprender a linguagem Node.js e a outra resolver o desafio.

#Conteúdos buscados para resolver o desafio:
* Cursos de Node.js - Alura
* Documentação de bibliotecas para ler arquivos: https://nodejs.org/api/readline.html
* E alguns conteúdos na internet, por exemplo: intender um pouco mais sobre callbacks -> https://imasters.com.br/desenvolvimento/gerenciando-o-fluxo-assincrono-de-operacoes-em-nodejs/?trace=1519021197&source=single

#O projeto:
- Para atender o desafio, foram feitos dois métodos GET: 
* Sendo o primeiro para listar informações agrupadas de todos os jogos (Task 1).
* Sendo o segundo para listar informações agrupadas de um único jogo (Task 2).

Além das informações de totais de kills, jogadores e kills por jogadores(como exemplo acima), foi adicionando também
a hora de início e fim dos jogos.

###Regras de negócio:
1. Só é contabilizado kills caso o jogador mate outro jogador, pois a momentos que o jogar matasse ele mesmo.
2. E só é descontando kills quando <world> mata o jogador(como descrito no desafio), mesmo se ele se matar não é descontado kills.

##Rodando o projeto:
1. Com o projeto na sua máquina(computador ou notebook), instale o Node.js(https://nodejs.org/en/) caso não tenha.
2. Após a instalação, entre na pasta raiz do projeto e abra um prompt de comando(atalho: ctrl + shift + botão direito do mouse/ escolha a opção para abrir o prompt).
3. Em seguida digite: "node index.js", esse arquivo é a startup da aplicação.
4. No prompt aparecerá algumas vezes a frase "Servidor rodando na porta 5000". Obs: Essa frase se repete pela quantidade de núcleos que seu processador tiver. 
Esse foi mais um adendo que eu fiz na aplicação para torná-la mais escalável. Caso queira intender, abra o arquivo index.js que terá uma pequena documentação explicando.
5. Pronto aplicação rodando!

##Utilizando os métodos GET:
1. Para fazer requisições para aplicação você precisará abrir outro terminal e utilizar por exemplo o comando CURL (que não é meu forte ainda), 
ou utilizar algum plugin como o Postaman (https://www.getpostman.com/)que abstrai o uso de chamadas HTTP através de botões e formulários.
Sua instalação é bem simples.

2. Para que a aplicação retorne todos os jogos, você deve usar o verbo GET para a url -> http://localhost:5000/quakelog. Depois 
adicione um parâmetro no Header: "filename/games.log". Esse parâmetro é o nome do arquivo onde está todas as informações de log
do Quake 3 Arena. A não informações dele retornará um erro que o arquivo não existe com o status 404.

3. Para que a aplicação retorne um jogo específico, você deve usar o verbo GET para a url -> http://localhost:5000/quakelog/id. Depois 
adicione um parâmetro no Header: "filename/games.log". Esse parâmetro é o nome do arquivo onde está todas as informações de log
do Quake 3 Arena. A não informações dele retornará um erro que o arquivo não existe com o status 404. O parâmetro id deve ser
um número inteiro.

##Testes unitários:
Para rodar os testes unitários basta você ir na pasta raiz do projeto, abrir o prompt nessa pasta e digitar: "node .\node_modules\mocha\bin\mocha", no prompt mesmo
você verá os testes que passaram ou os que falharam. 

##Reforçando Observações:
* Para ambas chamadas dos métodos deve-se se passar no hearder o nome do arquivo "games.log"

###Sugestões não implementadas:
* Substituir os substring e splits por expressão regular.





var fs = require('fs');
var readline = require('readline');

var _game = { startGame: "", endOfGame: "", players: [], total_kills: 0, kills: {} };
var _games = [];
var _idGamer = 0;

/**
 * <summary>
 * Inicio do game.
 * Método que abre o arquivo e lê linha por linha do arquivo.
 * Assim são destinadas a outros métodos para extrair as informações do jogo
 * </summary>
 * <returns>Uma Lista de informações agrupadas dos jogos</returns>  
 */
function startGame(nameFile, callback) {

    var rl = readline.createInterface({
        //input <stream.Readable> The Readable stream to listen to. This option is required.
        input: fs.createReadStream('./files/' + nameFile)
    });

    rl.on('line', function (line) {
        var parameter = line.split(':')[1];
        if (parameter)
            var valueParamenter = parameter.substr(3, (parameter.length - 3));


        switch (valueParamenter) {
            case 'InitGame':
                getstartGame(line);
                break;
            case 'ClientUserinfoChanged':
                getPlayers(line);
                break;
            case 'Kill':
                getKills(line);
                break;
            case 'ShutdownGame':
                finishGame(line);
            default:
                break;
        }

    }).on('close', function () {
        var endGame = _games;
        rl.close();
        clearObjects();
        return callback(endGame);
    });
}

/**
 * <summary>
 * Extrai os jogadores do game. 
 * </summary>
 * <param name="row">Linha do arquivo onde será extraído os jogadores</param>  
 */
function getPlayers(row) {
    var initPlayer = row.substr(row.indexOf('n\\') + 2, row.length).trim();
    var playerOfGame = initPlayer.split('\\')[0].trim();
    //Verifica se o jogodor já foi adicionado
    if (_game.players.indexOf(playerOfGame) == -1)
        _game.players.push(playerOfGame);
}

/**
 * <summary>
 * Extrai os jogadores que mataram e os que morreram. 
 * Adiciona também quantas mortes teve no jogo
 * </summary>
 * <param name="row">Linha do arquivo onde será extraído os jogadores</param>  
 */
function getKills(row) {
    _game.total_kills++;

    var killer = row.split(':')[3].split("killed")[0].trim();
    var dead = row.split(':')[3].split("killed")[1].split("by")[0].trim();

    //Adicionar kills para o jogador
    addKills(killer, dead);

    //Remover kills do jogador
    removeKills(killer, dead);
}

/**
 * <summary>
 * Adiciona kills para o jogador que matou, caso ele não seja o WORLD 
 * e se não for ele que matou ele mesmo (suicídio).
 * </summary>
 * <param name="killer">Jogador que receberá as kills</param>  
 * <param name="dead">Jogador que morreu</param>  
 */
function addKills(killer, dead) {
    if (killer != "<world>" && killer != dead) {
        if (_game.kills[killer]) {
            _game.kills[killer] += 1;
        }
        else {
            _game.kills[killer] = 1;
        }
    }
}

/**
 * <summary>
 * Remove kills do jogador que morreu, se foi o WORLD que o matou 
 * </summary>
 * <param name="dead">Jogador que recebera uma kill a menos</param>  
 * <param name="killer">Jogador que matou</param>  
 */
function removeKills(killer, dead) {
    if (killer == "<world>") {
        if (_game.kills[dead]) {
            _game.kills[dead] = _game.kills[dead] - 1;
        }
        else {
            _game.kills[dead] = -1;
        }
    }
}

/**
 * <summary>
 * Ao término da leitura do arquivo
 * essa função é chamada para limpar os objetos 
 * </summary>
 */
function clearObjects() {
    _idGamer = 0;
    _games = [];
    _game = { startGame: "", endOfGame: "", players: [], total_kills: 0, kills: {} };
}

/**
 * <summary>
 * Quando esse método é invocado é que a partida terminou.
 * Assim adiciona a partida em um lista de jogos, juntamente com o id da partida.
 * E esvazia(limpa) o outro objeto para que ele recebe um nova partida.
 * E também extrai a hora final da partida.
 * </summary>
 * <param name="row">Linha do arquivo onde será extraído a hora final da partida</param>  
 */
function finishGame(row) {
    _idGamer++;
    _game.endOfGame = row.trim().split(" ")[0];
    _games.push({ ["game_" + _idGamer]: _game });
    _game = { startGame: "", endOfGame: "", players: [], total_kills: 0, kills: {} };
}

/**
 * <summary>
 * Extrai a hora inicial da partida
 * </summary>
 * <param name="row">Linha do arquivo onde será extraído a hora inicial da partida</param>  
 */
function getstartGame(row) {
    _game.startGame = row.trim().split(" ")[0];
}

/**
 * <summary>
 * Disponibilizando para outros arquivos que queiram utilizar
 * No nosso caso a controllers
 * </summary>
 */
module.exports = function (nameArquivo, result) {
    return startGame(nameArquivo, result);
}
